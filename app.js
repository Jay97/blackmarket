//variables

const cartBtn = document.querySelector(".cart-btn");
const closeCartBtn = document.querySelector(".close-cart");
const clearCartBtn = document.querySelector(".clear-cart");
const cartDOM = document.querySelector(".cart");
const cartOverlay = document.querySelector(".cart-overlay");
const cartItems = document.querySelector(".cart-items");
const cartContent = document.querySelector(".cart-content");
const cartTotal = document.querySelector(".cart-total");
const productsDOM = document.querySelector(".products-center");

var bool = 1;

function toggleSideBar() {
  if (bool === 1) {
    document.getElementById("cartId").style.right = "0px";
    bool = 0;
  } else if (bool == 0) {
    document.getElementById("cartId").style.right = "-350px";
    // document.getElementById("cartId").style.marginRight = "350px";
    bool = 1;
  }
}
function untoggleSideBar() {
  //console.log(document.getElementById("cartId").style.right);
  document.getElementById("cartId").style.marginRight = "-350px";
}

function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  // document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  // document.getElementById("main").style.marginLeft = "0";
}

var countItem = 1;
function plusItem() {
  countItem++;
  document.getElementsByClassName("itemAmount")[0].innerHTML = countItem;
  console.log(countItem);
}

function minusItem() {
  countItem--;
  if (countItem < 0) {
    document.getElementsByClassName("itemAmount")[0].innerHTML = "0";
  } else {
    document.getElementsByClassName("itemAmount")[0].innerHTML = countItem;
  }
  console.log(countItem);
}

var itemAmount = document.getElementsByClassName("itemAmout");
for (var i = 0; i < itemAmount.length; i++) {
  console.log(itemAmount[i]);
}

var bagbtn = document.getElementsByClassName("remove-item");
console.log(bagbtn);
for (var i = 0; i < bagbtn.length; i++) {
  var button = bagbtn[i];
  button.addEventListener("click", removeCartItem);
}

var addToCartButton = document.getElementsByClassName("bag-btn");
for (var i = 0; i < addToCartButton.length; i++) {
  var button = addToCartButton[i];
  button.addEventListener("click", addToCartFunction);
}
var count = 0;
function addToCart() {
  count++;
  document.getElementById("cartItemID").innerHTML = count;
  // console.log(count);
}

function addToCartFunction(event) {
  var button = event.target;
  var shopItem = button.parentElement.parentElement;
  var title = shopItem.getElementsByClassName("itemTitle")[0].innerText;
  var price = shopItem.getElementsByClassName("itemPrice")[0].innerText;
  var imgSrc = shopItem.getElementsByClassName("product-img")[0].src;
  console.log(title, price, imgSrc);
  addItemToCart(title, price, imgSrc);
}

function addItemToCart(title, price, imgSrc) {
  var cartRow = document.createElement("div");
  cartRow.classList.add("cart-item");
  var cartItems = document.getElementsByClassName("cart-content")[0];
  var cartItemName = document.getElementsByClassName("cart-item-title");
  for (var i = 0; i < cartItemName.length; i++) {
    if (cartItemName[i].innerText == title) {
      alert("This item is already in the cart ");
      return;
    }
  }
  var cartRowContent = `
                          <img src="${imgSrc}" alt="download.immagine" />
                          <div>
                            <h4 class="cart-item-title"> ${title} </h4>
                            <h5 id="price"> ${price} </h5>
                            <span class="remove-item"> remove </span>
                          </div>  
  <div>
    <button class="chevronUpIcon" onclick="plusItem()">
      <i class="fas fa-chevron-up"></i>
    </button>
    <div id="itemAmount">1</div>
    <button class="chevronDownIcon" onclick="minusItem()">
      <i class="fas fa-chevron-down"></i>
    </button>
  </div>`;
  cartRow.innerHTML = cartRowContent;
  cartItems.append(cartRow);
  cartRow
    .getElementsByClassName("remove-item")[0]
    .addEventListener("click", removeCartItem);
}

function removeCartItem(event) {
  var buttonClicked = event.target;
  buttonClicked.parentElement.parentElement.remove();
}
