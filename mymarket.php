<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Welcome to Your Market </title>
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <?php
    $nome = $_POST['firstname'];
    $cognome = $_POST['lastname'];
    $email = $_POST['youremail'];

    echo '<p> Welcome to Your Market ' .
        htmlspecialchars($nome, ENT_QUOTES, 'UTF-8') . ' ' .
        htmlspecialchars($cognome, ENT_QUOTES, 'UTF-8') . ' ! </p>';

    ?>
</body>

</html>